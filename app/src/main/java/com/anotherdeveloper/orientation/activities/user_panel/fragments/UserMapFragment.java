package com.anotherdeveloper.orientation.activities.user_panel.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anotherdeveloper.orientation.R;
import com.anotherdeveloper.orientation.models.ScrollableMapView;
import com.anotherdeveloper.orientation.sensors.LocationManagement;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import butterknife.ButterKnife;

public class UserMapFragment extends SupportMapFragment implements OnMapReadyCallback, android.location.LocationListener, ScrollableMapView.IMapInitializator, View.OnClickListener {
    //TODO: Tags managing

    LocationManagement locationManagement;
    LocationManager locationManager;
    String locationProviderName;
    Location mLocation;
    GoogleMap mGoogleMap;
    View mapFragment;
    FloatingActionButton myLocation;
    double latitude;
    double longitude;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mapFragment = inflater.inflate(R.layout.map_fragment, container, false);
        ButterKnife.bind(this, mapFragment);
        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        locationManagement = LocationManagement.getInstance();
        locationProviderName = locationManagement.GetLocationProviderName(locationManager);
        return mapFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myLocation = (FloatingActionButton) getActivity().findViewById(R.id.my_location_FAB);
        myLocation.setOnClickListener(this);
        if (ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        initialize();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getActivity());
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mGoogleMap.setBuildingsEnabled(false);

        mGoogleMap.moveCamera(CameraUpdateFactory.zoomTo(13));
    }

    //    private boolean locationPermission() {
//        if (SDK_INT >= 23 && ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED
//                && ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED) {
//
//            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
//                    android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
//            return true;
//        }
    @Override
    public void initialize() {
        MapView mapView = (ScrollableMapView) mapFragment.findViewById(R.id.fragment_map);
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        locationProviderName = locationManagement.GetLocationProviderName(locationManager);
        //noinspection MissingPermission
        locationManager.requestLocationUpdates(locationProviderName, 1000, 0, this);

//        boolean network_enabled = locationManager.isProviderEnabled(locationProviderName);
//        if(network_enabled){
//            mLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//            if(location!=null){
//
//            }
//        }
        mLocation = locationManager.getLastKnownLocation(locationProviderName);
    }

    @Override
    public void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.my_location_FAB:
                locationManagement.getMyLocation(mGoogleMap, mLocation);
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLocation = location;
        latitude = mLocation.getLatitude();
        longitude = mLocation.getLongitude();
    }


    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {
        if (!isGPSEnabled()) {
            InitSnackbar("GPS provider disabled", "Settings", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent locationIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    locationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(locationIntent);
                }
            });

        }
        if (!isNetworkEnabled()) {
            InitSnackbar("Network provider disabled", "Settings", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent locationIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    locationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(locationIntent);
                }
            });
        }

    }

    private void InitSnackbar(String message, String actionName, View.OnClickListener onClickListener) {
        Snackbar.make(getActivity().findViewById(android.R.id.content), message, Snackbar.LENGTH_INDEFINITE)
                .setAction(actionName, onClickListener).show();
    }

    boolean isGPSEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    boolean isNetworkEnabled() {
        return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    /*@Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            Intent i = new Intent(getContext(), GPSService.class);
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getActivity().startService(i);
            } else {
                getActivity().stopService(i);
            }
        }
    }*/
}
