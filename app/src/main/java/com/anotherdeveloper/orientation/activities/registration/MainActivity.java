package com.anotherdeveloper.orientation.activities.registration;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.anotherdeveloper.orientation.R;
import com.anotherdeveloper.orientation.activities.AnimatedAppCompatActivity;
import com.anotherdeveloper.orientation.activities.login.LoginActivity;
import com.anotherdeveloper.orientation.activities.user_panel.UserActivity;
import com.anotherdeveloper.orientation.enums.FinishFlags;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public final class MainActivity extends AnimatedAppCompatActivity {
    FirebaseAuth firebaseAuth;
    @BindView(R.id.emailEditText)
    EditText emailEditText;
    @BindView(R.id.usernameEditText)
    EditText usernameEditText;
    @BindView(R.id.passwordEditText)
    EditText passwordEditText;
    private ProgressDialog progressDialog;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        FirebaseApp.initializeApp(getApplicationContext());
        progressDialog = new ProgressDialog(this);
        firebaseAuth = getFirebaseAuth();
        firebaseUser = getFirebaseUser();
        databaseReference = getDatabaseReference();

        if (firebaseUser != null) {
            OpenActivityWithFades(UserActivity.class, FinishFlags.FINISH_ACTIVITY);
        }
    }

    private void RegisterUser() {
        final String email = emailEditText.getText().toString().trim();
        final String password = passwordEditText.getText().toString().trim();
        final String username = usernameEditText.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, R.string.empty_mail, Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, R.string.empty_password, Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(username)) {
            Toast.makeText(this, R.string.empty_nickname, Toast.LENGTH_SHORT).show();
            return;
        }
        progressDialog.setMessage(getResources().getString(R.string.sign_up_process));
        progressDialog.show();

        Query query = databaseReference.child("Users").orderByChild("username").equalTo(username);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() > 0) {
                    Toast.makeText(MainActivity.this, "Username taken. Choose another.", Toast.LENGTH_SHORT).show();
                } else {
                    firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                UpdateUsername(email, username);
                                firebaseUser.sendEmailVerification();
                                firebaseAuth.signOut();
                            } else {
                                Toast.makeText(MainActivity.this, R.string.sign_up_failed, Toast.LENGTH_SHORT).show();
                            }

                        }
                    });

                }
                progressDialog.hide();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void UpdateUsername(String email, String username) {
        firebaseUser = firebaseAuth.getCurrentUser();

        if (firebaseUser != null) {
            String uid = firebaseUser.getUid();
            DatabaseReference currentRef = databaseReference.child("Users").child(uid);
            HashMap<String, String> map = new HashMap();
            map.put("email", email);
            map.put("username", username);
            currentRef.setValue(map);

            UserProfileChangeRequest uPCR = new UserProfileChangeRequest
                    .Builder()
                    .setDisplayName(username)
                    .build();
            firebaseUser.updateProfile(uPCR).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Toast.makeText(getApplicationContext(), R.string.sign_up_correct, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    @OnClick({R.id.registerButton, R.id.signInTextView})
    public void OnViewClicked(View view) {
        switch (view.getId()) {
            case R.id.registerButton:
                RegisterUser();
                break;
            case R.id.signInTextView:
                OpenActivityWithFades(LoginActivity.class, FinishFlags.FINISH_ACTIVITY);
                break;
        }
    }
}
