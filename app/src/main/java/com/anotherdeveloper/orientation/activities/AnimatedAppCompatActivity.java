package com.anotherdeveloper.orientation.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.anotherdeveloper.orientation.R;
import com.anotherdeveloper.orientation.enums.ActivityResults;
import com.anotherdeveloper.orientation.enums.FinishFlags;
import com.anotherdeveloper.orientation.models.CircleImageModel;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

public abstract class AnimatedAppCompatActivity extends AppCompatActivity {

    public void OpenActivityWithFades(Class activityClass, FinishFlags finishFlag) {
        switch (finishFlag) {
            case FINISH_ACTIVITY:
                finish();
                break;
            case HIDE_ACTIVITY:
                break;
        }

        overridePendingTransition(0, R.anim.fade_out);
        startActivity(new Intent(getApplicationContext(), activityClass));
    }

    public void StartService(Class serviceClass) {
        Intent i = new Intent(getApplicationContext(), serviceClass);
        startService(i);
    }

    public void StopService(Class serviceClass) {
        Intent i = new Intent(getApplicationContext(), serviceClass);
        stopService(i);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public void OpenActivityForResult(ActivityResults result) {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, result.getId());
    }

    public void SetImage(StorageReference storageReference, FirebaseUser firebaseUser, Uri selectedImage, final Transformation transformation, final ImageView imageView) {


        StorageReference filepath = storageReference.child(firebaseUser.getUid()).child("profilePhotos").child("profilePhoto");
        filepath.putFile(selectedImage).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Toast.makeText(getApplicationContext(), "Profile photo changed", Toast.LENGTH_SHORT).show();
                Uri profilePhotoUri = taskSnapshot.getDownloadUrl();
                LoadImageToView(getApplicationContext(), profilePhotoUri, transformation, imageView);

            }
        });
    }

    public void DownloadImage(StorageReference storageReference, FirebaseUser firebaseUser, final ImageView profilePhoto) {

        StorageReference filepath = storageReference
                .child(firebaseUser.getUid()).child("profilePhotos").child("profilePhoto");
        if (filepath != null) {
            filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    LoadImageToView(getApplicationContext(), uri, new CircleImageModel(), profilePhoto);
                    Log.d("selectedImgDownload", String.valueOf(uri));
                }
            });
        }
    }


    public static void LoadImageToView(Context context, Uri uri, Transformation transformation, ImageView imageView) {
        Picasso.with(context)
                .load(uri)
                .transform(transformation)
                .error(R.drawable.ic_add_a_photo_black_48dp)
                .into(imageView);
    }

    protected void initializeBottomSheetDialog(int layout) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getApplicationContext());
        bottomSheetDialog.setContentView(layout);
        bottomSheetDialog.setCancelable(true);
    }


    public FirebaseUser getFirebaseUser() {
        return getFirebaseAuth().getCurrentUser();
    }

    public FirebaseAuth getFirebaseAuth() {
        return FirebaseAuth.getInstance();
    }

    public StorageReference getStorageReference() {
        return FirebaseStorage.getInstance().getReference();
    }

    public DatabaseReference getDatabaseReference() {
        return FirebaseDatabase.getInstance().getReference();
    }
}
