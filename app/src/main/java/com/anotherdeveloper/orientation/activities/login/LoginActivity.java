package com.anotherdeveloper.orientation.activities.login;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.anotherdeveloper.orientation.R;
import com.anotherdeveloper.orientation.activities.AnimatedAppCompatActivity;
import com.anotherdeveloper.orientation.activities.registration.MainActivity;
import com.anotherdeveloper.orientation.activities.user_panel.UserActivity;
import com.anotherdeveloper.orientation.enums.FinishFlags;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.crash.FirebaseCrash;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AnimatedAppCompatActivity {
    @BindView(R.id.emailLoginEditText)
    EditText emailLoginEditText;
    @BindView(R.id.passwordLoginEditText)
    EditText passwordLoginEditText;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        progressDialog = new ProgressDialog(this);
        firebaseAuth = FirebaseAuth.getInstance();

    }

    private void UserLogin() {
        String email = emailLoginEditText.getText().toString().trim();
        String password = passwordLoginEditText.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, R.string.empty_mail, Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, R.string.empty_password, Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog.setMessage(getResources().getString(R.string.auth_process));
        progressDialog.show();
        SignInAttempt(email, password);

    }

    private void SignInAttempt(String email, String password) {
        try {
            firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        checkIfEmailVerified();
                    } else {
                        Toast.makeText(LoginActivity.this, R.string.auth_failed, Toast.LENGTH_SHORT).show();
                    }
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            FirebaseCrash.report(e);
        }
    }


    @OnClick({R.id.loginButton, R.id.signUpTextView})
    public void onViewClicked(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.loginButton:
                UserLogin();
                break;
            case R.id.signUpTextView:
                OpenActivityWithFades(MainActivity.class, FinishFlags.FINISH_ACTIVITY);
                break;
        }
    }

    private void checkIfEmailVerified() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user.isEmailVerified()) {
            OpenActivityWithFades(UserActivity.class, FinishFlags.FINISH_ACTIVITY);
            Toast.makeText(LoginActivity.this, getResources().getString(R.string.auth_correct, user.getDisplayName()), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(LoginActivity.this, R.string.verification_failed, Toast.LENGTH_SHORT).show();
            FirebaseAuth.getInstance().signOut();
        }
    }

//    private boolean locationPermission(){
//        if(SDK_INT>=23 && ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED
//                && ContextCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_COARSE_LOCATION)
//                !=PackageManager.PERMISSION_GRANTED){
//
//            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
//                    android.Manifest.permission.ACCESS_COARSE_LOCATION},1);
//            return true;
//        }
//        return false;
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//
//        switch (requestCode){
//            case 1:
//                StartService(GPSService.class);
//                break;
//        }
//    }
}
