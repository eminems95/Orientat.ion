package com.anotherdeveloper.orientation.activities.new_route;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.anotherdeveloper.orientation.R;
import com.anotherdeveloper.orientation.activities.AnimatedAppCompatActivity;
import com.anotherdeveloper.orientation.activities.new_route.fragments.ConfirmNewRoute;
import com.anotherdeveloper.orientation.models.ScrollableMapView;
import com.anotherdeveloper.orientation.sensors.LocationManagement;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.FirebaseApp;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public final class NewRouteActivity extends AnimatedAppCompatActivity
        implements
        OnMapReadyCallback,
        android.location.LocationListener,
        ScrollableMapView.IMapInitializator,
        View.OnClickListener {

    LocationManagement locationManagement;
    LocationManager locationManager;
    String locationProviderName;
    Location mLocation = locationManager.getLastKnownLocation(locationProviderName);
    GoogleMap mGoogleMap;
    ArrayList<Marker> checkpointsList;
    @BindView(R.id.toolbar_new_route)
    Toolbar toolbar;
    @BindView(R.id.new_route_nested_scroll_view)
    NestedScrollView nestedScrollView;
    @BindView(R.id.route_details_container)
    LinearLayoutCompat routeDetailsContainers;
    @BindView(R.id.nameTextInputLayout)
    TextInputLayout nameTextInputLayout;
    @BindView(R.id.new_route_scrollable_map)
    MapView mapView;

    double latitude;
    double longitude;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_route);
        ButterKnife.bind(NewRouteActivity.this);
        setSupportActionBar(toolbar);
        FirebaseApp.initializeApp(getApplicationContext());

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManagement = LocationManagement.getInstance();
        locationProviderName = locationManagement.GetLocationProviderName(locationManager);

        checkpointsList = new ArrayList<>();

        initialize();
        initializeBottomSheetDialog(R.layout.confirm_bottom_sheet_layout);

        nestedScrollView.setFillViewport(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void initialize() {

        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(this);
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mGoogleMap.setBuildingsEnabled(false);
        mGoogleMap.moveCamera(CameraUpdateFactory.zoomTo(15));

        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        //noinspection MissingPermission
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10, 0, this);
        //noinspection MissingPermission
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 0, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        locationProviderName = locationManagement.GetLocationProviderName(locationManager);
        //noinspection MissingPermission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(locationProviderName, 0, 0, this);

//        boolean network_enabled = locationManager.isProviderEnabled(locationProviderName);
//        if(network_enabled){
//            mLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//            if(location!=null){
//
//            }
//        }
        mLocation = locationManager.getLastKnownLocation(locationProviderName);
    }

    @Override
    public void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        mLocation = location;
        latitude = mLocation.getLatitude();
        longitude = mLocation.getLongitude();
        locationManagement.getMyLocation(mGoogleMap, mLocation);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private LatLng getLatLng(double latitude, double longitude) {

        LatLng latLng = new LatLng(latitude, longitude);
        return latLng;
    }

    @OnClick({R.id.set_checkpoint_fab, R.id.confirm_route_fab})
    public void onClickInjection(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.set_checkpoint_fab:
                Marker marker = mGoogleMap.addMarker(new MarkerOptions().position(getLatLng(latitude, longitude)));
                Circle circle = mGoogleMap.addCircle(new CircleOptions().center(getLatLng(latitude, longitude)).radius(5.00));

                checkpointsList.add(marker);
                break;
            case R.id.confirm_route_fab:
                BottomSheetDialogFragment confirmNewRoute = new ConfirmNewRoute();
                confirmNewRoute.show(getSupportFragmentManager(), confirmNewRoute.getTag());
                break;
//            case R.id.dismiss_bottom_dialog_button:
//                bottomSheetDialog.dismiss();
//                break;
        }
    }

    /*public void recursiveLoopChildren(ViewGroup parent) {
        for (int i = parent.getChildCount() - 1; i >= 0; i--) {
            final View child = parent.getChildAt(i);
            if (child instanceof FoldingCell) {
                recursiveLoopChildren((ViewGroup) child);
                child.setOnClickListener(this);
                child.setTag(child.getTop());
            } else {
                if (child != null) {
                    // DO SOMETHING WITH VIEW
                }
            }
        }
    }*/

    /*@Override
    public void onClick(View view) {
        if(view instanceof FoldingCell) {
            FoldingCell foldingCell = (FoldingCell)view;
            ViewGroup parent = (ViewGroup) foldingCell.getParent();
            if(!foldingCell.isUnfolded()) {
                foldingCell.setTranslationY(-foldingCell.getY());
                for (int i = parent.getChildCount() - 1; i >= 0; i--){
                    final FoldingCell child = (FoldingCell) parent.getChildAt(i);
                    child.fold(true);
            }
                foldingCell.unfold(true);
            }
            else {
                foldingCell.fold(true);
                foldingCell.setTranslationY(-foldingCell.getY());
            }
        }
    }*/


    private boolean isEditTextEmpty(String text) {

        return text.length() < 0;
    }

    @Override
    public void onClick(View view) {

    }
}

