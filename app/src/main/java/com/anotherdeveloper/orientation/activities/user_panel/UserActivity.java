package com.anotherdeveloper.orientation.activities.user_panel;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.anotherdeveloper.orientation.R;
import com.anotherdeveloper.orientation.activities.AnimatedAppCompatActivity;
import com.anotherdeveloper.orientation.activities.login.LoginActivity;
import com.anotherdeveloper.orientation.activities.new_route.NewRouteActivity;
import com.anotherdeveloper.orientation.activities.user_panel.fragments.PhotosManagementDialogFragment;
import com.anotherdeveloper.orientation.adapters.FragmentsAdapter;
import com.anotherdeveloper.orientation.enums.ActivityResults;
import com.anotherdeveloper.orientation.enums.FinishFlags;
import com.anotherdeveloper.orientation.models.CircleImageModel;
import com.anotherdeveloper.orientation.models.NoSwipeViewPager;
import com.anotherdeveloper.orientation.sensors.NFCClass;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Transformation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserActivity extends AnimatedAppCompatActivity
        implements
        NFCClass.ISetTextInView,
        AppBarLayout.OnOffsetChangedListener {

    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    DatabaseReference databaseReference;
    StorageReference storageReference;
    Uri selectedImage;
    NfcAdapter nfcAdapter;
    NFCClass nfcClass;
    Transformation transformation;
    ImageView polymorphImageView;
    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.profilePhotoImageView)
    ImageView profilePhoto;
    @BindView(R.id.profile_toolbar)
    Toolbar toolbar;
    @BindView(R.id.background_header)
    ImageView backgroundHeader;
    @BindView(R.id.collapse_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.app_bar)
    AppBarLayout appBarLayout;
    @BindView(R.id.viewpager)
    NoSwipeViewPager viewPager;
    @BindView(R.id.nested_scroll_view)
    NestedScrollView nestedScrollView;
    @BindView(R.id.sliding_tabs)
    AHBottomNavigation slidingTabs;
    @BindView(R.id.profile_photo_constraint_layout)
    ConstraintLayout profilePhotoConstraintLayout;
    @BindView(R.id.profile_photo_space)
    ImageView profilePhotoSpace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        nfcClass = NFCClass.getInstance();

        appBarLayout.addOnOffsetChangedListener(this);
        AHBottomNavigationInit(slidingTabs);
        ViewPagerInit(slidingTabs, viewPager);
        nestedScrollView.setFillViewport(true);

        firebaseAuth = getFirebaseAuth();
        firebaseUser = getFirebaseUser();
        databaseReference = getDatabaseReference();
        storageReference = getStorageReference();

        ToolbarManagement();

        DownloadImage(storageReference, firebaseUser, profilePhoto);

        if (firebaseUser == null) {
            OpenActivityWithFades(LoginActivity.class, FinishFlags.FINISH_ACTIVITY);
        }
        nfcClass.isNFCEnabled(nfcAdapter, UserActivity.this);
        backgroundHeader.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.new_york_668616_960_720));
        collapsingToolbar.bringChildToFront(backgroundHeader);
        profilePhotoSpace.bringToFront();
        profilePhotoConstraintLayout.bringToFront();
        toolbar.bringToFront();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.logout:
                firebaseAuth.signOut();
                OpenActivityWithFades(LoginActivity.class, FinishFlags.FINISH_ACTIVITY);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        DrawCircle(profilePhotoSpace);
        nfcClass.EnableForegroundDispatchSystem(getApplicationContext(), nfcAdapter, UserActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        nfcClass.DisableForegroundDispatchSystem(nfcAdapter, UserActivity.this);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            DrawCircle(profilePhotoSpace);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        nfcClass.TagDispatcher(getApplicationContext(), intent);
        setText(R.id.nfc_text_view);
    }

    @OnClick({R.id.addGameFAB, R.id.profilePhotoImageView})
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.addGameFAB:
                OpenActivityWithFades(NewRouteActivity.class, FinishFlags.HIDE_ACTIVITY);
                break;
            case R.id.profilePhotoImageView:
                transformation = new CircleImageModel();
                polymorphImageView = profilePhoto;
                BottomSheetDialogFragment bottomSheetDialogFragment = new PhotosManagementDialogFragment();
                bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                OpenActivityForResult(ActivityResults.RESULT_LOAD_IMG);
                break;
        }
    }

    @Override
    public void setText(int res) {
        TextView textView = (TextView) findViewById(res);
        textView.setText(nfcClass.setText());
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        try {
            float ratio = (appBarLayout.getHeight() - slidingTabs.getHeight()) / slidingTabs.getHeight();
            slidingTabs.setY(coordinatorLayout.getHeight() - slidingTabs.getHeight() - verticalOffset / ratio);
        } catch (ArithmeticException e) {
            Log.e("ratio divide by 0", e.getMessage());
        }
    }

    public void ToolbarManagement() {

        if (firebaseUser != null) {
            String displayName = firebaseUser.getDisplayName();
            for (UserInfo userInfo : firebaseUser.getProviderData()) {
                if (displayName == null && userInfo.getDisplayName() != null) {
                    displayName = userInfo.getDisplayName();
                }
            }
            getSupportActionBar().setTitle(displayName);
        }
    }

    private void AHBottomNavigationInit(AHBottomNavigation bottomNavigation) {

        AHBottomNavigationAdapter navigationAdapter = new AHBottomNavigationAdapter(this, R.menu.bottom_nav_bar_menu);
        navigationAdapter.setupWithBottomNavigation(bottomNavigation);
        bottomNavigation.setTranslucentNavigationEnabled(true);
        bottomNavigation.setAccentColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
    }


    private void ViewPagerInit(AHBottomNavigation bottomNavigationView, final NoSwipeViewPager viewPager) {
        FragmentsAdapter adapter = new FragmentsAdapter(getSupportFragmentManager(), 3);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setPagingEnabled(false);

        bottomNavigationView.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                if (!wasSelected)
                    viewPager.setCurrentItem(position);
                return true;
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (isItemPicked(requestCode, resultCode, ActivityResults.RESULT_LOAD_IMG.getId())) {
                // Get the Image from data
                selectedImage = data.getData();
                Log.d("selectedImgUri", String.valueOf(selectedImage));
                SetImage(storageReference, firebaseUser, selectedImage, transformation, polymorphImageView);
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG)
                    .show();
        }
    }


    private boolean isItemPicked(int requestCode, int resultCode, int requestCodeResult) {
        return requestCode == requestCodeResult && resultCode == RESULT_OK;
    }

    private void DrawCircle(ImageView imageView) {
        int width = (int) getResources().getDimension(R.dimen.image_width);
        int height = (int) getResources().getDimension(R.dimen.image_width);
        float size = Math.min(width, height);

        Log.d("ImageSize", "Width: " + width + "Height: " + height);

        Bitmap canvasBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(canvasBitmap);

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        float r = size / 2f;
        canvas.drawCircle(r, r, r, paint);
        imageView.setImageBitmap(canvasBitmap);
    }
}
