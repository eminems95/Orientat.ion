package com.anotherdeveloper.orientation.activities.user_panel.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.anotherdeveloper.orientation.R;
import com.anotherdeveloper.orientation.models.UserModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class OrienteeringPlacesFragment extends Fragment implements View.OnClickListener {
    EditText changeNameET;
    EditText changeAgeET;
    EditText changeDescriptionET;
    Button saveBT;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.profile_settings_fragment, container, false);

        changeNameET = (EditText) view.findViewById(R.id.change_nickname);
        changeAgeET = (EditText) view.findViewById(R.id.change_age);
        changeDescriptionET = (EditText) view.findViewById(R.id.change_description);
        saveBT = (Button) view.findViewById(R.id.save_details);
        saveBT.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.save_details:
                SaveDetailsToDatabase();
                break;
        }
    }

    private void SaveDetailsToDatabase() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        String age = changeAgeET.getText().toString().trim();
        String description = changeDescriptionET.getText().toString().trim();

        UserModel userModel = new UserModel(age, description);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        databaseReference.child("Users").child(user.getUid()).child("details").setValue(userModel);
    }
}
