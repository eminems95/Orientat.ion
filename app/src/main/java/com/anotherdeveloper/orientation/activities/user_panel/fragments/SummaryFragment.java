package com.anotherdeveloper.orientation.activities.user_panel.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anotherdeveloper.orientation.R;
import com.anotherdeveloper.orientation.sensors.NFCClass;

public class SummaryFragment extends Fragment {
    TextView nfcTV;
    NFCClass nfcClass;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.summary_fragment, container, false);

        return view;
    }
}
