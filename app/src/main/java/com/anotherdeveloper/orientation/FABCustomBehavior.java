package com.anotherdeveloper.orientation;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.view.View;

public class FABCustomBehavior extends CoordinatorLayout.Behavior<FloatingActionButton> {
    public FABCustomBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, FloatingActionButton child, View dependency) {
        float y = dependency.getY();
        float fabPaddingBottom = child.getBottom();
        float percentage = fabPaddingBottom / parent.getHeight();
        child.setTranslationY((float) ((375 - y) / Math.pow(percentage, 2)));
        return false;
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, FloatingActionButton child, View dependency) {
        return dependency instanceof AppBarLayout;
    }
}
