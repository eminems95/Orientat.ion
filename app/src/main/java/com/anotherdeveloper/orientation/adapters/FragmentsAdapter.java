package com.anotherdeveloper.orientation.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.anotherdeveloper.orientation.activities.user_panel.fragments.OrienteeringPlacesFragment;
import com.anotherdeveloper.orientation.activities.user_panel.fragments.SummaryFragment;
import com.anotherdeveloper.orientation.activities.user_panel.fragments.UserMapFragment;

public class FragmentsAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;

    public FragmentsAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;

        switch (position) {
            case 0:
                fragment = new UserMapFragment();
                break;
            case 1:
                fragment = new SummaryFragment();
                break;
            case 2:
                fragment = new OrienteeringPlacesFragment();
                break;
            default:
                fragment = null;
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
