package com.anotherdeveloper.orientation.models;

import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;

public class RouteModel {
    private Integer checkpointsAmount;
    private ArrayList<Marker> markers;

    public RouteModel(Integer checkpointsAmount, ArrayList<Marker> markers) {
        this.checkpointsAmount = checkpointsAmount;
        this.markers = markers;
    }
}

