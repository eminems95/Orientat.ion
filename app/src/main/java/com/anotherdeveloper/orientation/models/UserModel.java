package com.anotherdeveloper.orientation.models;

public class UserModel {
    private String userAge;
    private String description;

    public UserModel(String userAge,String description){
        this.userAge = userAge;
        this.description = description;
    }
}
