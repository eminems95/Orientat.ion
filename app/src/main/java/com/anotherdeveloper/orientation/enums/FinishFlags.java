package com.anotherdeveloper.orientation.enums;

public enum FinishFlags {
    FINISH_ACTIVITY,
    HIDE_ACTIVITY
}
