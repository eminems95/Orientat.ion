package com.anotherdeveloper.orientation.enums;

public enum ActivityResults {
    RESULT_LOAD_IMG(1);

    private final int id;

    ActivityResults(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
