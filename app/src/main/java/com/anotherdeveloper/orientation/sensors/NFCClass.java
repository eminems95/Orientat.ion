package com.anotherdeveloper.orientation.sensors;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;

public class NFCClass {

    private static NFCClass instance;
    private String tagContent = null;

    public NFCClass() {
    }

    public static NFCClass getInstance() {
        if (instance == null)
            instance = new NFCClass();

        return instance;
    }

    public void isNFCEnabled(NfcAdapter nfcAdapter, final Activity activity) {
        if (nfcAdapter != null && nfcAdapter.isEnabled()) {

        } else {
            Snackbar.make(activity.findViewById(android.R.id.content), "NFC disabled", Snackbar.LENGTH_INDEFINITE)
                    .setAction("Turn on", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            activity.startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
                        }
                    })
                    .show();
        }
    }

    public void TagDispatcher(Context context, Intent intent) {
        if (intent.hasExtra(NfcAdapter.EXTRA_TAG)) {

            Parcelable[] parcelables = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (parcelables != null && parcelables.length > 0) {
                ReadTextFromMessage((NdefMessage) parcelables[0]);
            } else {
                Toast.makeText(context, "No NDEF message found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void ReadTextFromMessage(NdefMessage ndefMessage) {
        NdefRecord[] ndefRecords = ndefMessage.getRecords();

        if (ndefRecords != null && ndefRecords.length > 0) {
            NdefRecord ndefRecord = ndefRecords[0];
            tagContent = GetTextFromNdefRecord(ndefRecord);
        } else {
            return;
        }
    }

    public void EnableForegroundDispatchSystem(Context context, NfcAdapter nfcAdapter, AppCompatActivity activity) {
        Intent intent = new Intent(context, activity.getClass());
        intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        IntentFilter[] intentFilter = new IntentFilter[]{};
        nfcAdapter.enableForegroundDispatch(activity, pendingIntent, intentFilter, null);
    }

    public void DisableForegroundDispatchSystem(NfcAdapter nfcAdapter, AppCompatActivity activity) {
        nfcAdapter.disableForegroundDispatch(activity);
    }

    private String GetTextFromNdefRecord(NdefRecord ndefRecord) {
        String tagContent = null;
        try {
            byte[] payload = ndefRecord.getPayload();
            String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";
            int languageSize = payload[0] & 0063;
            tagContent = new String(payload, languageSize + 1,
                    payload.length - languageSize - 1, textEncoding);
        } catch (UnsupportedEncodingException e) {
            Log.e("Unsupported Encoding", e.getMessage(), e);
        }
        return tagContent;
    }

    public String setText() {
        return tagContent;
    }

    public interface ISetTextInView {
        void setText(int res);
    }
}
