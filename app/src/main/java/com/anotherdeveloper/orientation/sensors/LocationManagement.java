package com.anotherdeveloper.orientation.sensors;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;


public class LocationManagement {
    private static LocationManagement instance;

    public static LocationManagement getInstance() {
        if (instance == null)
            instance = new LocationManagement();

        return instance;
    }

    private LocationManagement() {
    }

    public String GetLocationProviderName(LocationManager lManager) {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setBearingRequired(true);
        return lManager.getBestProvider(criteria, false);
    }

    public void getMyLocationFirstTime(GoogleMap mGoogleMap, Location location) {
        if (location != null) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 7));
        }
    }


    public void getMyLocation(GoogleMap mGoogleMap, Location location) {
        if (location != null) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        }
    }
}
